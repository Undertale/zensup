<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Mail extends Mailable
{
    use Queueable, SerializesModels;

    public $suivi;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Array $suivi)
    {
        $this->suivi = $suivi;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['address' => 'dylan.ajpi@ajpi.fr', 'name' => 'Dylan Adam'])->view('emails.suivi');
    }
}
