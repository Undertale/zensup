<?php

namespace App\Http\Controllers;

use App\Group;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function modForm () {
        $user = Auth::user();

        return view ('account', [
            'user' => $user,
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
        ]);
    }

    public function postModForm (Request $request) {

        $user = User::find($request->get('id'));

        $avatar = $request->file('avatar');
        if ($avatar) {
            $avatar->move('upload', $avatar->getClientOriginalName());
            $user->avatar = '/upload/' . $avatar->getClientOriginalName();
        } else {
            $user->avatar = $user->avatar;
        }

        $user->name = $request->get('name');
        $user->fname = $request->get('fname');
        $user->email = $request->get('email');

        if ($request->get('password')) {
            $user->password = Hash::make($request->get('password'));
        } else {
            $user->password = $user->password;
        }

        $user->phone = $request->get('phone');
        $user->mobile = $request->get('mobile');

        $user->society = $request->get('society');
        $user->group = $request->get('group');
        $user->remember_token = null;

        $user->save();

        return redirect('/home')->with('Status', 'Compte modifié !');
    }
}
