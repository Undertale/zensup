<?php

namespace App\Http\Controllers;

use App\Categorie;
use App\Emergency;
use App\Group;
use App\Impact;
use App\Priority;
use App\Stat;
use App\Ticket;
use App\Trace;
use App\Type;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PhpParser\Node\Expr\New_;

class TicketController extends Controller
{
    public function indexYourTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => $user,
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('applicant', '=', Auth::id())
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexSTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => Auth::User(),
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('A.society', '=', $user->society)
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexATicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => Auth::User(),
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexYourNewTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => Auth::User(),
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('applicant', '=', Auth::id())->where('S.wording', 'LIKE', 'Nouveau')
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexYourProgressTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => Auth::User(),
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('applicant', '=', Auth::id())->where('S.wording', 'LIKE', 'En cours')
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexYourResolvTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => Auth::User(),
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('applicant', '=', Auth::id())->where('S.wording', 'LIKE', 'Résolu')
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexYourRejectTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => Auth::User(),
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('applicant', '=', Auth::id())->where('S.wording', 'LIKE', 'Rejeté')
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexSNewTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => $user,
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('A.society', '=', $user->society)->where('S.wording', 'LIKE', 'Nouveau')
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexSProgressTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => Auth::User(),
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('A.society', '=', $user->society)->where('S.wording', 'LIKE', 'En cours')
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexSResolvTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => Auth::User(),
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('A.society', '=', $user->society)->where('S.wording', 'LIKE', 'Résolu')
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexSRejectTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => Auth::User(),
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('A.society', '=', $user->society)->where('S.wording', 'LIKE', 'Rejeté')
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexANewTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => $user,
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('S.wording', 'LIKE', 'Nouveau')
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexAProgressTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => Auth::User(),
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('S.wording', 'LIKE', 'En cours')
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexAResolvTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => Auth::User(),
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('S.wording', 'LIKE', 'Résolu')
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function indexARejectTicket () {
        $user = Auth::User();

        return view ('ticket.ticket', [
            'user' => Auth::User(),
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'tickets' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->where('S.wording', 'LIKE', 'Rejeté')
                ->get(
                    [
                        'ticket.id AS id',
                        'title',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'S.wording AS stat', 'S.color AS sColor',
                        'P.wording AS priority', 'P.color AS pColor',
                        'A.name AS applicantName', 'A.fname AS applicantFname',
                        'T.name AS techName', 'T.fname AS techFname',
                        'C.wording AS categorie',
                        'ticket.time_resolv AS time_resolv'
                    ])
        ]);
    }

    public function formAdd () {
        $user = Auth::User();

        return view ('ticket.add', [
            'user' => $user,
            'type' => Type::get(['id', 'wording']),
            'categorie' => Categorie::get(['id', 'wording']),
            'emergency' =>  Emergency::get(['id', 'wording']),
            'stat' => Stat::all(),
            'impact' => Impact::all(),
            'priority' => Priority::all(),
            'technician' => User::join('groups', 'users.group', '=', 'groups.id')->where('wording', 'LIKE', 'administrator')->get(['*', 'users.id AS id']),
            'applicant' => User::all(),
            'group' => Group::where('id', '=', $user->group)->first(['wording'])
        ]);
    }

    public function store (Request $request) {
        $t = new Ticket();

        if ($request->get('time_PEC') or $request->get('time_PEC') == " ") {
            $t->time_PEC = $request->get('time_PEC');
        } else {
            $t->time_PEC = NULL;
        }

        if ($request->get('time_resolv') or $request->get('time_resolv') == " ") {
            $t->time_resolv = $request->get('time_resolv');
        } else {
            $t->time_resolv = NULL;
        }

        $t->type = $request->get('type');
        $t->categories = $request->get('categorie');

        if ($request->get('applicant')) {
            $t->applicant = $request->get('applicant');
            $user = User::find($request->get('applicant'));
        } else {
            $t->applicant = Auth::id();
            $user = User::find(Auth::id());
        }

        if ($request->get('technician')) {
            $t->technician = $request->get('technician');
        } else {
            $technician = User::where('group', '=', '1')->inRandomOrder()->first(['id']);
            $t->technician = $technician->id;
        }

        if ($request->get('stat')) {
            $t->stat = $request->get('stat');
        } else {
            $stat = Stat::find(1);
            $t->stat = $stat->id;
        }

        if ($request->get('emergency')) {
            $t->emergency = $request->get('emergency');
        } else {
            $emergency = Emergency::find(1);
            $t->emergency = $emergency->id;
        }

        if ($request->get('impact')) {
            $t->impact = $request->get('impact');
        } else {
            $impact = Impact::find(1);
            $t->impact = $impact->id;
        }

        if ($request->get('priority')) {
            $t->priority = $request->get('priority');
        } else {
            $priority = Priority::find(1);
            $t->priority = $priority->id;
        }

        $t->title = $request->get('title');
        $t->description = $request->get('description');

        $file = $request->file("file");
        if ($file) {
            $file->move('upload', $file->getClientOriginalName());
            $t->file = '/upload/' . $file->getClientOriginalName();
        } else {
            $t->file = 'pas de fichier joint';
        }
        $t->save();

        $emails = [$user->email];
        $supervisor = User::join('groups', 'users.group', '=', 'groups.id')->where('society', '=', $user->society)->where('groups.wording', '=', 'supervisor')->get();
        foreach ($supervisor as $s) {
            $emails[] = $s->email;
        }
        $admin = User::join('groups', 'users.group', '=', 'groups.id')->where('groups.wording', '=', 'administrator')->get();
        foreach ($admin as $a) {
            $emails[] = $a->email;

            $ticket  = Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->join('type', 'ticket.type', '=', 'type.id')
                ->join('emergency', 'ticket.emergency', '=', 'emergency.id')
                ->join('impact', 'ticket.impact', '=', 'impact.id')
                ->where('ticket.id', '=', $t->id)
                ->first(
                    [
                        'ticket.id AS id',
                        'title', 'description',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'type.wording AS type', 'type.id AS type_id',
                        'S.wording AS stat', 'S.color AS sColor', 'S.id AS stat_id',
                        'P.wording AS priority', 'P.color AS pColor', 'P.id AS priority_id',
                        'A.name AS applicantName', 'A.fname AS applicantFname', 'A.id AS applicant_id',
                        'T.name AS techName', 'T.fname AS techFname', 'T.id AS tech_id',
                        'C.wording AS categorie', 'C.id AS categorie_id',
                        'ticket.time_resolv AS time_resolv',
                        'emergency.wording AS emergency', 'emergency.id AS emergency_id',
                        'impact.wording AS impact', 'impact.id AS impact_id',
                        'time_PEC', 'file']);

            $trace = Trace::join('users', 'trace.user', '=', 'users.id')
                ->where('ticket', '=', $t->id)
                ->orderBy('trace.created_at', 'DESC')
                ->get(['*', 'trace.created_at AS created_at']);
        }

        //envoie de mail pour le suivi
        Mail::send('emails.suivi', ['ticket' => $ticket, 'trace' => $trace], function ($message) use ($emails) {
            $message->to($emails)->subject('Création de ticket');
            $message->from('dylan.adam@ajpi.fr', 'ADAM Dylan');
        });

        return redirect('/home')->with('Status', 'Ticket ouvert !');
    }

    public function suppr(Request $request) {
        $ticket = Ticket::find($request->id);
        $ticket->delete();

        return redirect('/ticket')->with('Status', 'Ticket supprimer !');
    }

    public function formMod(Request $request) {
        $user = Auth::User();

        return view ('ticket.mod', [
            'user' => $user,
            'type' => Type::get(['id', 'wording']),
            'categorie' => Categorie::get(['id', 'wording']),
            'emergency' =>  Emergency::get(['id', 'wording']),
            'stat' => Stat::all(),
            'impact' => Impact::all(),
            'priority' => Priority::all(),
            'technician' => User::join('groups', 'users.group', '=', 'groups.id')->where('wording', 'LIKE', 'administrator')->get(['*', 'users.id AS id']),
            'applicant' => User::all(),
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'ticket' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
            ->join('priority AS P', 'ticket.priority', '=', 'P.id')
            ->join('users AS A', 'ticket.applicant', '=', 'A.id')
            ->join('users AS T', 'ticket.technician', '=', 'T.id')
            ->join('categories AS C', 'ticket.categories', '=', 'C.id')
            ->join('type', 'ticket.type', '=', 'type.id')
            ->join('emergency', 'ticket.emergency', '=', 'emergency.id')
            ->join('impact', 'ticket.impact', '=', 'impact.id')
            ->where('ticket.id', '=', $request->id)
            ->first(
                [
                    'ticket.id AS id',
                    'title', 'description',
                    'ticket.updated_at AS updated_at',
                    'ticket.created_at AS created_at',
                    'type.wording AS type', 'type.id AS type_id',
                    'S.wording AS stat', 'S.color AS sColor', 'S.id AS stat_id',
                    'P.wording AS priority', 'P.color AS pColor', 'P.id AS priority_id',
                    'A.name AS applicantName', 'A.fname AS applicantFname', 'A.id AS applicant_id',
                    'T.name AS techName', 'T.fname AS techFname', 'T.id AS tech_id',
                    'C.wording AS categorie', 'C.id AS categorie_id',
                    'ticket.time_resolv AS time_resolv',
                    'emergency.wording AS emergency', 'emergency.id AS emergency_id',
                    'impact.wording AS impact', 'impact.id AS impact_id',
                    'time_PEC'
                ])
        ]);
    }

    public function modStore (Request $request) {
        $t = Ticket::find($request->id);

        if ($request->get('time_PEC') or $request->get('time_PEC') == " ") {
            $t->time_PEC = $request->get('time_PEC');
        } else {
            $t->time_PEC = NULL;
        }

        if ($request->get('time_resolv') or $request->get('time_resolv') == " ") {
            $t->time_resolv = $request->get('time_resolv');
        } else {
            $t->time_resolv = NULL;
        }

        $t->type = $request->get('type');

        $t->categories = $request->get('categorie');

        if ($request->get('applicant')) {
            $t->applicant = $request->get('applicant');
            $user = User::find($request->get('applicant'));
        } else {
            $t->applicant = Auth::id();
            $user = User::find(Auth::id());
        }

        if ($request->get('technician')) {
            $t->technician = $request->get('technician');
        } else {
            $technician = User::where('group', '=', '1')->inRandomOrder()->first(['id']);
            $t->technician = $technician->id;
        }

        if ($request->get('stat')) {
            $t->stat = $request->get('stat');
        } else {
            $stat = Stat::find(1);
            $t->stat = $stat->id;
        }

        if ($request->get('emergency')) {
            $t->emergency = $request->get('emergency');
        } else {
            $emergency = Emergency::find(1);
            $t->emergency = $emergency->id;
        }

        if ($request->get('impact')) {
            $t->impact = $request->get('impact');
        } else {
            $impact = Impact::find(1);
            $t->impact = $impact->id;
        }

        if ($request->get('priority')) {
            $t->priority = $request->get('priority');
        } else {
            $priority = Priority::find(1);
            $t->priority = $priority->id;
        }

        $t->title = $request->get('title');
        $t->description = $request->get('description');

        $file = $request->file("file");
        if ($file) {
            $file->move('upload', $file->getClientOriginalName());
            $t->file = '/upload/' . $file->getClientOriginalName();
        } else {
            $t->file = $t->file;
        }
        $t->save();


        $emails = [$user->email];
        $supervisor = User::join('groups', 'users.group', '=', 'groups.id')->where('society', '=', $user->society)->where('groups.wording', '=', 'supervisor')->get();
        foreach ($supervisor as $s) {
            $emails[] = $s->email;
        }
        $admin = User::join('groups', 'users.group', '=', 'groups.id')->where('groups.wording', '=', 'administrator')->get();
        foreach ($admin as $a) {
            $emails[] = $a->email;
        }

        $ticket  = Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
            ->join('priority AS P', 'ticket.priority', '=', 'P.id')
            ->join('users AS A', 'ticket.applicant', '=', 'A.id')
            ->join('users AS T', 'ticket.technician', '=', 'T.id')
            ->join('categories AS C', 'ticket.categories', '=', 'C.id')
            ->join('type', 'ticket.type', '=', 'type.id')
            ->join('emergency', 'ticket.emergency', '=', 'emergency.id')
            ->join('impact', 'ticket.impact', '=', 'impact.id')
            ->where('ticket.id', '=', $request->id)
            ->first(
                [
                    'ticket.id AS id',
                    'title', 'description',
                    'ticket.updated_at AS updated_at',
                    'ticket.created_at AS created_at',
                    'type.wording AS type', 'type.id AS type_id',
                    'S.wording AS stat', 'S.color AS sColor', 'S.id AS stat_id',
                    'P.wording AS priority', 'P.color AS pColor', 'P.id AS priority_id',
                    'A.name AS applicantName', 'A.fname AS applicantFname', 'A.id AS applicant_id',
                    'T.name AS techName', 'T.fname AS techFname', 'T.id AS tech_id',
                    'C.wording AS categorie', 'C.id AS categorie_id',
                    'ticket.time_resolv AS time_resolv',
                    'emergency.wording AS emergency', 'emergency.id AS emergency_id',
                    'impact.wording AS impact', 'impact.id AS impact_id',
                    'time_PEC', 'file']);

        $trace = Trace::join('users', 'trace.user', '=', 'users.id')
            ->where('ticket', '=', $request->id)
            ->orderBy('trace.created_at', 'DESC')
            ->get(['*', 'trace.created_at AS created_at']);

        //envoie de mail pour le suivi
        Mail::send('emails.suivi', ['ticket' => $ticket, 'trace' => $trace], function ($message) use ($emails, $request) {
            $message->to($emails)->subject('Modification du ticket #' . $request->id);
            $message->from('dylan.adam@ajpi.fr', 'ADAM Dylan');
        });

        return redirect('/home')->with('Status', 'Ticket modifier !');
    }

    public function view (Request $request) {
        $user = Auth::User();

        return view ('ticket.view', [
            'user' => $user,
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
            'trace' => Trace::join('users', 'trace.user', '=', 'users.id')
                ->where('ticket', '=', $request->id)
                ->orderBy('trace.created_at', 'DESC')
                ->get(['*', 'trace.created_at AS created_at']),
            'ticket' => Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
                ->join('priority AS P', 'ticket.priority', '=', 'P.id')
                ->join('users AS A', 'ticket.applicant', '=', 'A.id')
                ->join('users AS T', 'ticket.technician', '=', 'T.id')
                ->join('categories AS C', 'ticket.categories', '=', 'C.id')
                ->join('type', 'ticket.type', '=', 'type.id')
                ->join('emergency', 'ticket.emergency', '=', 'emergency.id')
                ->join('impact', 'ticket.impact', '=', 'impact.id')
                ->where('ticket.id', '=', $request->id)
                ->first(
                    [
                        'ticket.id AS id',
                        'title', 'description',
                        'ticket.updated_at AS updated_at',
                        'ticket.created_at AS created_at',
                        'type.wording AS type', 'type.id AS type_id',
                        'S.wording AS stat', 'S.color AS sColor', 'S.id AS stat_id',
                        'P.wording AS priority', 'P.color AS pColor', 'P.id AS priority_id',
                        'A.name AS applicantName', 'A.fname AS applicantFname', 'A.id AS applicant_id',
                        'T.name AS techName', 'T.fname AS techFname', 'T.id AS tech_id',
                        'C.wording AS categorie', 'C.id AS categorie_id',
                        'ticket.time_resolv AS time_resolv',
                        'emergency.wording AS emergency', 'emergency.id AS emergency_id',
                        'impact.wording AS impact', 'impact.id AS impact_id',
                        'time_PEC', 'file'
                    ])
        ]);
    }
}
