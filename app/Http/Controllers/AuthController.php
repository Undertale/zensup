<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index() {
        return view ('auth.login');
    }

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('/')->withErrors($validator, 'login')->withInput();
        }

        if(!Auth::attempt(['email' => $request->input('email'), 'password' => $request->input('password')])) {
            return redirect('/')->withErrors(['auth' => 'Identifiant ou mot de passe incorrect !'])->withInput();
        } else {
            return redirect('/home');
        }
    }

    public function formForgot() {
        return view ('auth.passwords.email');
    }

    public function logout() {
        Auth::logout();
        return redirect('/');
    }
}
