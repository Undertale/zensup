<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InventoryController extends Controller
{
    public function index () {
        $dirname = "inventory";
        $nb_file = 0;

        if ($dir = opendir($dirname)) {
            while(false !== ($file = readdir($dir))) {
                if($file != '.' && $file != '..' && $file != 'index.php') {
                    $nb_file++;
                    $filesList[] = $file;
                }
            }
        }

        $user = Auth::user();

        return view ('inventory', [
            'files' => $filesList,
            'user' => $user,
            'group' => Group::where('id', '=', $user->group)->first(['wording']),
        ]);
    }
}
