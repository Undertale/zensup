<?php

namespace App\Http\Controllers;

use App\Ticket;
use App\Trace;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class TraceController extends Controller
{
    public function store(Request $request) {
        $trace = new Trace();

        $trace->user = Auth::id();
        $trace->ticket = $request->get('ticket');
        $trace->description = $request->get('description');
        $trace->save();

        $user = User::find(Auth::id());
        $emails = [$user->email];
        $supervisor = User::join('groups', 'users.group', '=', 'groups.id')->where('society', '=', $user->society)->where('groups.wording', '=', 'supervisor')->get();
        foreach ($supervisor as $s) {
            $emails[] = $s->email;
        }
        $admin = User::join('groups', 'users.group', '=', 'groups.id')->where('groups.wording', '=', 'administrator')->get();
        foreach ($admin as $a) {
            $emails[] = $a->email;
        }

        $ticket  = Ticket::join('stat AS S', 'ticket.stat', '=', 'S.id')
            ->join('priority AS P', 'ticket.priority', '=', 'P.id')
            ->join('users AS A', 'ticket.applicant', '=', 'A.id')
            ->join('users AS T', 'ticket.technician', '=', 'T.id')
            ->join('categories AS C', 'ticket.categories', '=', 'C.id')
            ->join('type', 'ticket.type', '=', 'type.id')
            ->join('emergency', 'ticket.emergency', '=', 'emergency.id')
            ->join('impact', 'ticket.impact', '=', 'impact.id')
            ->where('ticket.id', '=', $request->get('ticket'))
            ->first(
                [
                    'ticket.id AS id',
                    'title', 'description',
                    'ticket.updated_at AS updated_at',
                    'ticket.created_at AS created_at',
                    'type.wording AS type', 'type.id AS type_id',
                    'S.wording AS stat', 'S.color AS sColor', 'S.id AS stat_id',
                    'P.wording AS priority', 'P.color AS pColor', 'P.id AS priority_id',
                    'A.name AS applicantName', 'A.fname AS applicantFname', 'A.id AS applicant_id',
                    'T.name AS techName', 'T.fname AS techFname', 'T.id AS tech_id',
                    'C.wording AS categorie', 'C.id AS categorie_id',
                    'ticket.time_resolv AS time_resolv',
                    'emergency.wording AS emergency', 'emergency.id AS emergency_id',
                    'impact.wording AS impact', 'impact.id AS impact_id',
                    'time_PEC', 'file']);

        $trace = Trace::join('users', 'trace.user', '=', 'users.id')
            ->where('ticket', '=', $request->get('ticket'))
            ->orderBy('trace.created_at', 'DESC')
            ->get(['*', 'trace.created_at AS created_at']);

        //envoie de mail pour le suivi
        Mail::send('emails.suivi', ['ticket' => $ticket, 'trace' => $trace], function ($message) use ($emails, $request) {
            $message->to($emails)->subject('Ajout d\'une solution au ticket #' . $request->get('ticket'));
            $message->from('dylan.adam@ajpi.fr', 'ADAM Dylan');
        });

        return back();
    }
}
