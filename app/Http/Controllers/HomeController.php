<?php

namespace App\Http\Controllers;

use App\Group;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index() {
        $user = Auth::user();

        return view ('home', [
            'user' => $user,
            'group' => Group::where('id', '=', $user->group)->first(['wording'])
        ]);
    }
}
