<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('time_PEC');
            $table->dateTime('time_resolv');
            $table->integer('type')->unsigned();
            $table->integer('categories')->unsigned();
            $table->integer('applicant')->unsigned();
            $table->integer('technician')->unsigned();
            $table->integer('stat')->unsigned();
            $table->integer('emergency')->unsigned();
            $table->integer('impact')->unsigned();
            $table->integer('priority')->unsigned();
            $table->string('title');
            $table->string('description');
            $table->string('file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket');
    }
}
