<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->foreign('group')->references('id')->on('groups');
            $table->foreign('society')->references('id')->on('society');
        });

        Schema::table('ticket', function (Blueprint $table) {
            $table->foreign('type')->references('id')->on('type');
            $table->foreign('categories')->references('id')->on('categories');
            $table->foreign('applicant')->references('id')->on('users');
            $table->foreign('technician')->references('id')->on('users');
            $table->foreign('stat')->references('id')->on('stat');
            $table->foreign('emergency')->references('id')->on('emergency');
            $table->foreign('impact')->references('id')->on('impact');
            $table->foreign('priority')->references('id')->on('priority');
        });

        Schema::table('trace', function (Blueprint $table) {
            $table->foreign('ticket')->references('id')->on('ticket');
            $table->foreign('user')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
