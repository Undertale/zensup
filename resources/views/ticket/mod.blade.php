@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">

                @if ($group->wording == 'administrator')
                    <div class="card-box table-responsive">
                        <h4 class="text-center header-title m-t-0 m-b-30">Modifier le ticket {{ $ticket->id }}</h4>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                @include('form.modAdminTicket')
                            </div>
                        </div>
                    </div>
                @endif
                <div class="card-box table-responsive">
                    <h4 class="text-center header-title m-t-0 m-b-30">Ajout d'une solution</h4>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            @include('form.trace')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection