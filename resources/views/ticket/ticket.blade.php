@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box table-responsive">

                    <h4 class="header-title m-t-0 m-b-30">Tous vos tickets</h4>
                    <a href="{{ route('addTicket') }}" class="btn btn-info waves-effect w-md waves-light m-b-5"><i class="fa fa-plus" aria-hidden="true"></i> Créer un ticket</a>
                    <div class="m-b-20"></div>

                    <table id="datatable" class="table table-bordered table-condensed">
                        <thead>
                            <tr>
                                <th class="text-center">ID</th>
                                <th class="text-center">Titre</th>
                                <th class="text-center">Statut</th>
                                <th class="text-center">Dernière modification</th>
                                <th class="text-center">Date d'ouverture</th>
                                <th class="text-center">Priorité</th>
                                <th class="text-center">Demandeur</th>
                                <th class="text-center">Attribué à</th>
                                <th class="text-center">Catégorie</th>
                                <th class="text-center">Temps de résolution</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>

                        <tbody class="text-center">
                            @foreach($tickets as $t)
                                <tr>
                                    <td>{{ $t->id }}</td>
                                    <td>{{ $t->title }}</td>
                                    <td><span style="color: {{ $t->sColor }}"><i class="fa fa-circle" aria-hidden="true"></i></span> {{ $t->stat }}</td>
                                    <td>{{ $t->updated_at }}</td>
                                    <td>{{ $t->created_at }}</td>
                                    <td style="background-color: {{ $t->pColor }}">{{ $t->priority }}</td>
                                    <td>{{ $t->applicantName }} {{ $t->applicantFname }}</td>
                                    <td>{{ $t->techName }} {{ $t->techFname }}</td>
                                    <td>{{ $t->categorie }}</td>
                                    <td>{{ $t->time_resolv }}</td>
                                    <td>
                                        <a href="{{ route('viewTicket', ['id' => $t->id]) }}" class="btn btn-icon waves-effect waves-light btn-primary btn-sm m-b-5"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                        <a href="{{ route('modifyTicket', ['id' => $t->id]) }}" class="btn btn-icon waves-effect waves-light btn-success btn-sm m-b-5"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                        @if ($group->wording == 'administrator')
                                            <a href="javascript:if(confirm('&Ecirc;tes-vous sûr de vouloir supprimer ?')) document.location.href='{{ route('deleteTicket', ['id' => $t->id]) }}'" class="btn btn-icon waves-effect waves-light btn-danger btn-sm m-b-5"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
     </div>
@endsection