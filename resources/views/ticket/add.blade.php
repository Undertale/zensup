@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box table-responsive">
                    <h4 class="text-center header-title m-t-0 m-b-30">Créer un ticket</h4>
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            @if ($group->wording == 'user' or $group->wording == 'supervisor')
                                @include('form.addUserTicket')
                            @elseif ($group->wording == 'administrator')
                                @include('form.addAdminTicket')
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection