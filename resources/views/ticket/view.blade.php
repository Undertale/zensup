@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box table-responsive">
                    <h4 class="header-title m-t-0 m-b-30">Détails du ticket n°{{ $ticket->id }}</h4>

                    @if($group->wording == 'administrator')
                        <?php $color = '#44525A'; ?>
                    @else
                        <?php $color = '#F4F8FB'; ?>
                    @endif

                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td style="background-color:{{ $color }};">Date d'ouverture : </td>
                            <td colspan="3">{{ $ticket->created_at }}</td>
                        </tr>
                        <tr>
                            <td style="background-color:{{ $color }};">Temps PEC : </td>
                            <td>{{ $ticket->time_PEC }}</td>
                            <td style="background-color:{{ $color }};">Temps de résolution : </td>
                            <td>{{ $ticket->time_resolv }}</td>
                        </tr>
                        <tr>
                            <td style="background-color:{{ $color }};">Type : </td>
                            <td>{{ $ticket->type }}</td>
                            <td style="background-color:{{ $color }};">Catégorie : </td>
                            <td>{{ $ticket->categorie }}</td>
                        </tr>
                        <tr>
                            <td style="background-color:{{ $color }};">Statut : </td>
                            <td colspan="3">{{ $ticket->stat }}</td>
                        </tr>
                        <tr>
                            <td style="background-color:{{ $color }};">Urgence : </td>
                            <td colspan="3">{{ $ticket->emergency }}</td>
                        </tr>
                        <tr>
                            <td style="background-color:{{ $color }};">Impact : </td>
                            <td colspan="3">{{ $ticket->impact }}</td>
                        </tr>
                        <tr>
                            <td style="background-color:{{ $color }};">Priorité : </td>
                            <td colspan="3">{{ $ticket->priority }}</td>
                        </tr>
                        <tr>
                            <td style="background-color:{{ $color }};">Titre : </td>
                            <td colspan="3">{{ $ticket->title }}</td>
                        </tr>
                        <tr>
                            <td colspan="4" style="background-color:{{ $color }};">Description : </td>
                        </tr>
                        <tr>
                            <td colspan="4">{!! $ticket->description !!}</td>
                        </tr>
                        <tr>
                            <td colspan="4" style="background-color:{{ $color }};">Fichier joint : </td>
                        </tr>
                        <tr><td colspan="4"><img src="{{ $ticket->file }}" alt="piece jointe"></td></tr>

                        <tr>
                           <td colspan="4" style="background-color:{{ $color }};">Solution : </td>
                        </tr>
                        @foreach ($trace as $t)
                            <tr>
                                <td colspan="1">{{ $t->name }} {{ $t->fname }}</td>
                                <td colspan="2">{!! $t->description !!}</td>
                                <td colspan="1">{{ $t->created_at }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection