@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card-box table-responsive">
                    <h4 class="text-center header-title m-t-0 m-b-30">Inventaire des machines</h4>

                    <table id="datatable" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nom du fichier</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($files as $f)
                                <tr><td><a style="color: white;" href="/inventory/{{ $f }}">{{ $f }}</a></td></tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection