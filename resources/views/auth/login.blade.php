<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="WebStudio">

    <!-- App Favicon -->
    <link rel="shortcut icon" href="/images/favicon.ico">

    <!-- App title -->
    <title>ZenSup - Gestionnaire de ticket par AJPI</title>

    <!-- App CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/css/core.css" rel="stylesheet" type="text/css" />
    <link href="/css/components.css" rel="stylesheet" type="text/css" />
    <link href="/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="/css/menu.css" rel="stylesheet" type="text/css" />
    <link href="/css/responsive.css" rel="stylesheet" type="text/css" />

    <script src="/js/modernizr.min.js"></script>

</head>
<body>

<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="wrapper-page">
    <div class="text-center">
        <a href="http://www.ajpi.pro" class="logo text-info">ZenSup</a>
        <h5 class="text-muted m-t-0 font-600">Gestionnaire de ticket par AJPI</h5>
    </div>
    <div class="m-t-40 card-box">
        <div class="text-center">
            <h4 class="text-uppercase font-bold m-b-0">Connexion</h4>
        </div>
        <div class="panel-body">
            <form class="form-horizontal m-t-20" action="{{ route('default') }}" method="POST">
                {{ csrf_field() }}
                @if($errors->any())
                    <div class="alert alert-danger">
                        {{ $errors->first('auth') }}
                    </div>
                @endif
                <div class="form-group">
                    <div class="col-xs-12">
                        <input id="email" class="form-control @if ($errors->login->first('email')) parsley-error @endif" type="text" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>

                        @if ($errors->login->first('email'))
                            <p class="text-danger">Email non valide !</p>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <input id="password" class="form-control" type="password" placeholder="Mot de passe" name="password" required>
                    </div>
                </div>

                <div class="form-group text-center m-t-30">
                    <div class="col-xs-12">
                        <button class="btn btn-bordred btn-info btn-block waves-effect waves-light" type="submit">Connexion</button>
                    </div>
                </div>

                <div class="form-group m-t-30 m-b-0">
                    <div class="col-sm-12">
                        <a href="{{ route('forgotPass') }}" class="text-muted"><i class="fa fa-lock m-r-5"></i> Mot de passe oublié ?</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <!-- end card-box-->

    <div class="row">
        <div class="col-sm-12 text-center">
            <p class="text-muted">Je n'ai pas encore de compte ? <a href="" class="text-info m-l-5"><b>Inscrivez-vous</b></a></p>
        </div>
    </div>

</div>
<!-- end wrapper page -->



<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/detect.js"></script>
<script src="/js/fastclick.js"></script>
<script src="/js/jquery.slimscroll.js"></script>
<script src="/js/jquery.blockUI.js"></script>
<script src="/js/waves.js"></script>
<script src="/js/wow.min.js"></script>
<script src="/js/jquery.nicescroll.js"></script>
<script src="/js/jquery.scrollTo.min.js"></script>

<!-- App js -->
<script src="/assets/js/jquery.core.js"></script>
<script src="/assets/js/jquery.app.js"></script>

</body>
</html>