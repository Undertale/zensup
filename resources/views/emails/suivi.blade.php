<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Suivi ticket</title>
</head>
<body>
    <table style="width: 100%; border: 1px solid #EBEFF2; max-width: 100%; background-color: transparent; border-collapse: collapse">
        <tbody style="color: #797979;">
        <tr>
            <td style="background-color: #f4f8fb; border: 1px solid #EBEFF2; padding: 8px;">Date d'ouverture : </td>
            <td style="border: 1px solid #EBEFF2; padding: 8px;" colspan="3">{{ $ticket->created_at }}</td>
        </tr>
        <tr>
            <td style="background-color: #f4f8fb; border: 1px solid #EBEFF2; padding: 8px;">Temps PEC : </td>
            <td style="border: 1px solid #EBEFF2; padding: 8px;">{{ $ticket->time_PEC }}</td>
            <td style="background-color: #f4f8fb; border: 1px solid #EBEFF2; padding: 8px;">Temps de résolution : </td>
            <td style="border: 1px solid #EBEFF2; padding: 8px;">{{ $ticket->time_resolv }}</td>
        </tr>
        <tr>
            <td style="background-color: #f4f8fb; border: 1px solid #EBEFF2; padding: 8px;">Type : </td>
            <td style="border: 1px solid #EBEFF2; padding: 8px;">{{ $ticket->type }}</td>
            <td style="background-color: #f4f8fb; border: 1px solid #EBEFF2; padding: 8px;">Catégorie : </td>
            <td style="border: 1px solid #EBEFF2; padding: 8px;">{{ $ticket->categorie }}</td>
        </tr>
        <tr>
            <td style="background-color: #f4f8fb; border: 1px solid #EBEFF2; padding: 8px;">Statut : </td>
            <td style="border: 1px solid #EBEFF2; padding: 8px;" colspan="3">{{ $ticket->stat }}</td>
        </tr>
        <tr>
            <td style="background-color: #f4f8fb; border: 1px solid #EBEFF2; padding: 8px;">Urgence : </td>
            <td style="border: 1px solid #EBEFF2; padding: 8px;" colspan="3">{{ $ticket->emergency }}</td>
        </tr>
        <tr>
            <td style="background-color: #f4f8fb; border: 1px solid #EBEFF2; padding: 8px;">Impact : </td>
            <td style="border: 1px solid #EBEFF2; padding: 8px;" colspan="3">{{ $ticket->impact }}</td>
        </tr>
        <tr>
            <td style="background-color: #f4f8fb; border: 1px solid #EBEFF2; padding: 8px;">Priorité : </td>
            <td style="border: 1px solid #EBEFF2; padding: 8px;" colspan="3">{{ $ticket->priority }}</td>
        </tr>
        <tr>
            <td style="background-color: #f4f8fb; border: 1px solid #EBEFF2; padding: 8px;">Titre : </td>
            <td style="border: 1px solid #EBEFF2; padding: 8px;" colspan="3">{{ $ticket->title }}</td>
        </tr>
        <tr>
            <td colspan="4" style="background-color: #f4f8fb; border: 1px solid #EBEFF2; padding: 8px;">Description : </td>
        </tr>
        <tr>
            <td style="border: 1px solid #EBEFF2; padding: 8px;" colspan="4">{!! $ticket->description !!}</td>
        </tr>
        <tr>
            <td colspan="4" style="background-color: #f4f8fb; border: 1px solid #EBEFF2; padding: 8px;">Fichier joint : </td>
        </tr>
        <tr><td style="border: 1px solid #EBEFF2; padding: 8px;" colspan="4"><img src="{{ $ticket->file }}" alt="piece jointe"></td></tr>

        <tr>
            <td colspan="4" style="background-color: #f4f8fb; border: 1px solid #EBEFF2; padding: 8px;">Solution : </td>
        </tr>
        @foreach ($trace as $t)
            <tr>
                <td style="border: 1px solid #EBEFF2; padding: 8px;" colspan="1">{{ $t->name }} {{ $t->fname }}</td>
                <td style="border: 1px solid #EBEFF2; padding: 8px;" colspan="2">{!! $t->description !!}</td>
                <td style="border: 1px solid #EBEFF2; padding: 8px;" colspan="1">{{ $t->created_at }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</body>
</html>