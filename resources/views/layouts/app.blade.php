<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ZenSup - Gestionnaire de ticket par AJPI</title>
    @if ($group->wording == 'administrator')
        <link href="/dark/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet"/>
        <link href="/dark/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css"/>
        <link href="/dark/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
        <link href="/dark/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
        <link href="/dark/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet"/>
        <link href="/dark/plugins/switchery/switchery.min.css" rel="stylesheet"/>
        <link href="/dark/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="/dark/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
        <link href="/dark/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
        <link href="/dark/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">


        <!-- DataTables -->
            <link href="/dark/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
            <link href="/dark/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="/dark/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="/dark/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="/dark/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
            <link href="/dark/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
            <link href="/dark/css/core.css" rel="stylesheet" type="text/css" />
            <link href="/dark/css/components.css" rel="stylesheet" type="text/css" />
            <link href="/dark/css/icons.css" rel="stylesheet" type="text/css" />
            <link href="/dark/css/pages.css" rel="stylesheet" type="text/css" />
            <link href="/dark/css/menu.css" rel="stylesheet" type="text/css" />
            <link href="/dark/css/responsive.css" rel="stylesheet" type="text/css" />

            <!-- form Uploads -->
            <link href="/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />
    @else
            <link href="/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet"/>
            <link href="/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css"/>
            <link href="/plugins/select2/dist/css/select2.css" rel="stylesheet" type="text/css">
            <link href="/plugins/select2/dist/css/select2-bootstrap.css" rel="stylesheet" type="text/css">
            <link href="/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet"/>
            <link href="/plugins/switchery/switchery.min.css" rel="stylesheet"/>
            <link href="/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
            <link href="/plugins/mjolnic-bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
            <link href="/plugins/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css" rel="stylesheet">
            <link href="/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

            <!-- DataTables -->
            <link href="/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css"/>
            <link href="/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <link href="/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <link href="/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <link href="/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet" type="text/css"/>

            <!-- form Uploads -->
            <link href="/plugins/fileuploads/css/dropify.min.css" rel="stylesheet" type="text/css" />

            <!-- App css -->
            <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
            <link href="/css/core.css" rel="stylesheet" type="text/css"/>
            <link href="/css/components.css" rel="stylesheet" type="text/css"/>
            <link href="/css/icons.css" rel="stylesheet" type="text/css"/>
            <link href="/css/pages.css" rel="stylesheet" type="text/css"/>
            <link href="/css/menu.css" rel="stylesheet" type="text/css"/>
            <link href="/css/responsive.css" rel="stylesheet" type="text/css"/>
    @endif

    <script src="/js/modernizr.min.js"></script>

</head>

<body class="fixed-top">

<!-- Begin page -->
<div id="wrapper">
    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="{{ route('home') }}" class="logo text-info">ZenSup<i class="zmdi zmdi-layers"></i></a>
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">

                <!-- Page title -->
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <button class="button-menu-mobile open-left">
                            <i class="zmdi zmdi-menu"></i>
                        </button>
                    </li>
                    <li>
                        <h4 class="page-title">Gestionnaire de ticket AJPI</h4>
                    </li>
                </ul>

                <!-- Right(Notification and Searchbox -->
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <!-- Notification -->
                        <div class="notification-box">
                            <ul class="list-inline m-b-0">
                                <li>
                                    <a href="javascript:void(0);" class="right-bar-toggle">
                                        <i class="fa fa-question-circle-o"></i>
                                    </a>
                                    <div class="noti-dot">
                                        <span class="dot"></span>
                                        <span class="pulse"></span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- End Notification bar -->
                    </li>
                    <li class="hidden-xs">
                        <form role="search" class="app-search">
                            <input type="text" placeholder="Recherche..."
                                   class="form-control">
                            <a href=""><i class="fa fa-search"></i></a>
                        </form>
                    </li>
                </ul>

            </div><!-- end container -->
        </div><!-- end navbar -->
    </div>
    <!-- Top Bar End -->

    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">

            <!-- User -->
            <div class="user-box">
                <div class="user-img">
                    <img src="{{ $user->avatar }}" alt="user-img" title="" class="img-circle img-thumbnail img-responsive">
                    <div class="user-status online"><i class="zmdi zmdi-dot-circle"></i></div>
                </div>
                <h5><a href="#">{{ $user->name }} {{ $user->fname }}</a></h5>
                <ul class="list-inline">
                    <li><a href="{{ route('account') }}"><i class="zmdi zmdi-settings"></i></a></li>
                    <li><a href="{{ route('default') }}" class="text-danger"><i class="zmdi zmdi-power"></i></a></li>
                </ul>
            </div>
            <!-- End User -->

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <ul>
                    <li class="text-muted menu-title">Navigation</li>

                    <li>
                        <a href="{{ route('home') }}" class="waves-effect"><i class="zmdi zmdi-view-dashboard"></i>
                            <span> Tableau de bord </span>
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('addTicket') }}" class="waves-effect"><i class="fa fa-plus" aria-hidden="true"></i>
                            <span> Ouvrir un ticket </span>
                        </a>
                    </li>

                    <li class="has_sub">
                        <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-tags"></i> <span> Vos tickets </span> <span class="menu-arrow"></span></a>
                        <ul class="list-unstyled">
                            <li><a href="{{ route('ticket') }}">Tous les états</a></li>
                            <li><a href="{{ route('yTicketNew') }}">Nouveau</a></li>
                            <li><a href="{{ route('yTicketProgress') }}">En cours</a></li>
                            <li><a href="{{ route('yTicketResolv') }}">Résolu</a></li>
                            <li><a href="{{ route('yTicketReject') }}">Rejeté</a></li>
                        </ul>
                    </li>

                    @if ($group->wording == 'supervisor' or $group->wording == 'administrator')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-tags"></i> <span> tickets société </span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="{{ route('sTicket') }}">Tous les états</a></li>
                                <li><a href="{{ route('sTicketNew') }}">Nouveau</a></li>
                                <li><a href="{{ route('sTicketProgress') }}">En cours</a></li>
                                <li><a href="{{ route('sTicketResolv') }}">Résolu</a></li>
                                <li><a href="{{ route('sTicketReject') }}">Rejeté</a></li>
                            </ul>
                        </li>
                    @endif

                    @if ($group->wording == 'administrator')
                        <li class="has_sub">
                            <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-tags"></i> <span> Tous les tickets </span> <span class="menu-arrow"></span></a>
                            <ul class="list-unstyled">
                                <li><a href="{{ route('aTicket') }}">Tous les états</a></li>
                                <li><a href="{{ route('aTicketNew') }}">Nouveau</a></li>
                                <li><a href="{{ route('aTicketProgress') }}">En cours</a></li>
                                <li><a href="{{ route('aTicketResolv') }}">Résolu</a></li>
                                <li><a href="{{ route('aTicketReject') }}">Rejeté</a></li>
                            </ul>
                        </li>
                    @endif

                    @if ($group->wording == 'administrator')
                        <li>
                            <a href="{{ route('inventory') }}" class="waves-effect"><i class="fa fa-server" aria-hidden="true"></i>
                                <span> Inventaire </span>
                            </a>
                        </li>
                    @endif



                </ul>
                <div class="clearfix"></div>
            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>

    </div>
    <!-- Left Sidebar End -->

    <!-- content page -->
    <div class="content-page">
        <div class="content">
            @yield('content')
        </div>
    </div>
    <!-- end content page -->
</div>
<!-- End page -->

<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/detect.js"></script>
<script src="/js/fastclick.js"></script>
<script src="/js/jquery.blockUI.js"></script>
<script src="/js/waves.js"></script>
<script src="/js/jquery.nicescroll.js"></script>
<script src="/js/jquery.slimscroll.js"></script>
<script src="/js/jquery.scrollTo.min.js"></script>

<!-- Datatables-->
<script src="/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/plugins/datatables/dataTables.bootstrap.js"></script>
<script src="/plugins/datatables/dataTables.buttons.min.js"></script>
<script src="/plugins/datatables/buttons.bootstrap.min.js"></script>
<script src="/plugins/datatables/jszip.min.js"></script>
<script src="/plugins/datatables/pdfmake.min.js"></script>
<script src="/plugins/datatables/vfs_fonts.js"></script>
<script src="/plugins/datatables/buttons.html5.min.js"></script>
<script src="/plugins/datatables/buttons.print.min.js"></script>
<script src="/plugins/datatables/dataTables.fixedHeader.min.js"></script>
<script src="/plugins/datatables/dataTables.keyTable.min.js"></script>
<script src="/plugins/datatables/dataTables.responsive.min.js"></script>
<script src="/plugins/datatables/responsive.bootstrap.min.js"></script>
<script src="/plugins/datatables/dataTables.scroller.min.js"></script>

<!-- Datatable init js -->
<script src="/pages/datatables.init.js"></script>

<!-- Plugins Js -->
<script src="/plugins/switchery/switchery.min.js"></script>
<script src="plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>
<script type="text/javascript" src="/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="/plugins/select2/dist/js/select2.min.js" type="text/javascript"></script>
<script src="/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js"
        type="text/javascript"></script>
<script src="/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js" type="text/javascript"></script>
<script src="/plugins/moment/moment.js"></script>
<script src="/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<script src="/plugins/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script src="/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script src="/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>

<!-- file uploads js -->
<script src="/plugins/fileuploads/js/dropify.min.js"></script>

<!-- KNOB JS -->
<!--[if IE]>
<script type="text/javascript" src="/plugins/jquery-knob/excanvas.js"></script>
<![endif]-->
<script src="/plugins/jquery-knob/jquery.knob.js"></script>

<!-- Dashboard init -->
<script src="/pages/jquery.dashboard.js"></script>

<!-- App js -->
<script src="/js/jquery.core.js"></script>
<script src="/js/jquery.app.js"></script>

<!--form wysiwig js-->
<script src="/plugins/tinymce/tinymce.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        if ($("#description").length > 0) {
            tinymce.init({
                selector: "textarea#description",
                theme: "modern",
                height: 150,
                toolbar: 'styleselect | bold italic | forecolor backcolor | bullist numlist outdent indent | table link image | code fullscreen',
                menubar: false,
                style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ],
                plugins: [
                    'table directionality searchreplace',
                    'tabfocus autoresize link paste',
                    'textcolor colorpicker'
                ]
            });
        }
    });
</script>

<script>
    // Select2
    $(".select2").select2();

    $(".select2-limiting").select2({
        maximumSelectionLength: 2
    });

    // Date Picker
    jQuery('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        highlights: true
    });
</script>

<script>
    $(document).ready(function () {
        $('#datatable').dataTable({
            "language": {
                "lengthMenu": "Afficher _MENU_ tickets par page",
                "emptyTable": "Pas de données trouvées",
                "zeroRecords": "Aucun résultat trouvé",
                "info": "Page _PAGE_ sur _PAGES_",
                "infoEmpty": "Pas de ticket disponible",
                "search": "Recherche : ",
                "infoFiltered": "(filtrer dans _MAX_ tickets)",
                "paginate": {
                    "first": "<<",
                    "last": ">>",
                    "next": "Suivant",
                    "previous": "Précédent"
                }
            }
        });
    });
</script>

<script type="text/javascript">
    $('.dropify').dropify({
        messages: {
            'default': 'Glissez déposez votre fichier',
            'replace': 'Glissez déposé ou cliquez pour remplacer',
            'remove': 'Supprimer',
            'error': 'Ooops, une erreur est survenue'
        },
        error: {
            'fileSize': 'Le fichier est trop volumineux'
        }
    });
</script>
