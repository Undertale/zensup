{!! Form::open(['route' => 'storeTrace', 'method' => 'post']) !!}

{{ Form::input('text', 'ticket', $ticket->id, ['hidden']) }}

<div class="row p-t-10">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('description', 'Description : ') }}</div>
        <div class="col-md-10">
            <textarea id="description" name="description"></textarea>
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="col-md-12 text-center">
        <button type="submit" class="btn btn-success waves-effect w-md waves-light m-b-5"><i class="fa fa-floppy-o" aria-hidden="true"></i> Proposer</button>
    </div>
</div>

{!! Form::Close() !!}