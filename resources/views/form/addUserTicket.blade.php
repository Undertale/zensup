{!! Form::open(['route' => 'addTicket', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}

<div class="row">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('type', 'Type Demande : ') }}</div>
        <div class="col-md-4">
            <select name="type" class="form-control select2">
                @foreach ($type as $t)
                    <option value="{{ $t->id }}">{{ $t->wording }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('categorie', 'Categorie : ') }}</div>
        <div class="col-md-4">
            <select name="categorie" class="form-control select2">
                @foreach ($categorie as $c)
                    <option value="{{ $c->id }}">{{ $c->wording }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('emergency', 'Urgence : ') }}</div>
        <div class="col-md-4">
            <select name="emergency" class="form-control select2">
                @foreach ($emergency as $e)
                    <option value="{{ $e->id }}">{{ $e->wording }}</option>
                @endforeach
            </select>
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('title', 'Titre : ') }}</div>
        <div class="col-md-10">
            {!! Form::input('text', 'title', null, ['placeholder' => 'mon pc ne démarre plus !', 'class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('description', 'Description : ') }}</div>
        <div class="col-md-10">
            <textarea id="description" name="description"></textarea>
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('file', 'Pièce jointe : ') }}</div>
        <div class="col-md-10">
            {!! Form::input('file', 'file', null, ['class' => 'dropify']) !!}
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="col-md-12 text-center">
        <button type="submit" class="btn btn-success waves-effect w-md waves-light m-b-5"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</button>
        <a href="{{ route('home') }}" class="btn btn-danger waves-effect w-md waves-light m-b-5"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</a>
    </div>
</div>

{!! Form::close() !!}