{!! Form::open(['route' => 'postModAccount', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}

{{ Form::input('text', 'id', $user->id, ['hidden']) }}

<div class="row">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('avatar', 'Avatar : ') }}</div>
        <div class="col-md-8">
            {{ Form::input('file', 'avatar', null, ['class' => 'form-control']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group p-t-10">
        <div class="col-md-2">{{ Form::label('name', 'Nom : ') }}</div>
        <div class="col-md-4">
            {{ Form::input('text', 'name', $user->name, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group p-t-10">
        <div class="col-md-2">{{ Form::label('fname', 'Prénom : ') }}</div>
        <div class="col-md-4">
            {{ Form::input('text', 'fname', $user->fname, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group p-t-10">
        <div class="col-md-2">{{ Form::label('email', 'Email : ') }}</div>
        <div class="col-md-4">
            {{ Form::input('email', 'email', $user->email, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group p-t-10">
        <div class="col-md-2">{{ Form::label('password', 'Mot de passe : ') }}</div>
        <div class="col-md-4">
            {{ Form::input('password', 'password', null, ['class' => 'form-control', 'placeholder' => 'laissez vide pour garder l\'ancien mot de passe']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group p-t-10">
        <div class="col-md-2">{{ Form::label('phone', 'Ligne fixe : ') }}</div>
        <div class="col-md-4">
            {{ Form::input('text', 'phone', $user->phone, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group p-t-10">
        <div class="col-md-2">{{ Form::label('mobile', 'Téléphone mobile : ') }}</div>
        <div class="col-md-4">
            {{ Form::input('text', 'mobile', $user->mobile, ['class' => 'form-control', 'required']) }}
        </div>
    </div>
</div>

{{ Form::input('text', 'society', $user->society, ['hidden']) }}
{{ Form::input('text', 'group', $user->group, ['hidden']) }}

<div class="row p-t-10">
    <div class="col-md-12 text-center">
        <button type="submit" class="btn btn-success waves-effect w-md waves-light m-b-5"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</button>
        <a href="{{ route('home') }}" class="btn btn-danger waves-effect w-md waves-light m-b-5"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</a>
    </div>
</div>

{!! Form::close() !!}