{!! Form::open(['route' => 'postModTicket', 'method' => 'post', 'enctype' => 'multipart/form-data']) !!}

{{ Form::input('text', 'id', $ticket->id, ['hidden']) }}
<div class="row">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('created_at', 'Date d\'ouverture : ') }}</div>
        <div class="col-md-4">
            {{ Form::input('text', 'created_at', $ticket->created_at, ['class' => 'form-control', 'disabled']) }}
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="col-md-6">
        <div class="col-md-6 p-0">{{ Form::label('time_PEC', 'Temps PEC : ') }}</div>
        <div class="col-md-6 p-0">
            {{ Form::input('text', 'time_PEC', $ticket->time_PEC, ['class' => 'form-control datepicker']) }}
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-6 p-0">{{ Form::label('time_resolv', 'Temps résolution : ') }}</div>
        <div class="col-md-6 p-0">
            {{ Form::input('text', 'time_resolv', $ticket->time_resolv, ['class' => 'form-control datepicker']) }}
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="col-md-6">
        <div class="col-md-6 p-0">{{ Form::label('type', 'Type : ') }}</div>
        <div class="col-md-6 p-0">
            <select name="type" class="form-control select2">
                <option value="{{ $ticket->type_id }}">{{ $ticket->type }}</option>
                <optgroup label="----------">
                    @foreach ($type as $t)
                        <option value="{{ $t->id }}">{{ $t->wording }}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-6 p-0">{{ Form::label('categorie', 'Catégorie : ') }}</div>
        <div class="col-md-6 p-0">
            <select name="categorie" class="form-control select2">
                <option value="{{ $ticket->categorie_id }}">{{ $ticket->categorie }}</option>
                <optgroup label="-----------">
                    @foreach ($categorie as $c)
                        <option value="{{ $c->id }}">{{ $c->wording }}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="col-md-6">
        <div class="col-md-6 p-0">{{ Form::label('applicant', 'Demandeur : ') }}</div>
        <div class="col-md-6 p-0">
            <select name="applicant" class="form-control select2">
                <option value="{{ $ticket->applicant_id }}">{{ $ticket->applicantName }} {{ $ticket->applicantFname }}</option>
                <optgroup label="----------">
                    @foreach ($applicant as $a)
                        <option value="{{ $a->id }}">{{ $a->name }} {{ $a->fname }}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-6 p-0">{{ Form::label('technician', 'Technicien : ') }}</div>
        <div class="col-md-6 p-0">
            <select name="technician" class="form-control select2">
                <option value="{{ $ticket->tech_id }}">{{ $ticket->techName }} {{ $ticket->techFname }}</option>
                <optgroup label="----------">
                    @foreach ($technician as $tech)
                        <option value="{{ $tech->id }}">{{ $tech->name }} {{ $tech->fname }}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('statut', 'Statut : ') }}</div>
        <div class="col-md-4">
            <select name="stat" class="form-control select2">
                <option value="{{ $ticket->stat_id }}">{{ $ticket->stat }}</option>
                <optgroup label="----------">
                    @foreach ($stat as $s)
                        <option value="{{ $s->id }}">{{ $s->wording }}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('emergency', 'Urgence : ') }}</div>
        <div class="col-md-4">
            <select name="emergency" class="form-control select2">
                <option value="{{ $ticket->emergency_id }}">{{ $ticket->emergency }}</option>
                <optgroup label="----------">
                    @foreach ($emergency as $e)
                        <option value="{{ $e->id }}">{{ $e->wording }}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('impact', 'Impact : ') }}</div>
        <div class="col-md-4">
            <select name="impact" class="form-control select2">
                <option value="{{ $ticket->impact_id }}">{{ $ticket->impact }}</option>
                <optgroup label="----------">
                    @foreach ($impact as $i)
                        <option value="{{ $i->id }}">{{ $i->wording }}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('priority', 'Priorité : ') }}</div>
        <div class="col-md-4">
            <select name="priority" class="form-control select2">
                <option value="{{ $ticket->priority_id }}">{{ $ticket->priority }}</option>
                <optgroup label="----------">
                    @foreach ($priority as $p)
                        <option value="{{ $p->id }}">{{ $p->wording }}</option>
                    @endforeach
                </optgroup>
            </select>
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="col-md-2">{{ Form::label('title', 'Titre : ') }}</div>
    <div class="col-md-10">{{ Form::input('text', 'title', $ticket->title, ['placeholder' => 'mon pc ne démarre plus !', 'class' => 'form-control']) }}</div>
</div>

<div class="row p-t-10">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('description', 'Description : ') }}</div>
        <div class="col-md-10">
            <textarea id="description" name="description">{{ $ticket->description }}</textarea>
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="form-group">
        <div class="col-md-2">{{ Form::label('file', 'Pièce jointe : ') }}</div>
        <div class="col-md-10">
            {!! Form::input('file', 'file', null, ['class' => 'dropify']) !!}
        </div>
    </div>
</div>

<div class="row p-t-10">
    <div class="col-md-12 text-center">
        <button type="submit" class="btn btn-success waves-effect w-md waves-light m-b-5"><i class="fa fa-floppy-o" aria-hidden="true"></i> Enregistrer</button>
        <a href="{{ route('home') }}" class="btn btn-danger waves-effect w-md waves-light m-b-5"><i class="fa fa-arrow-left" aria-hidden="true"></i> Retour</a>
    </div>
</div>

{!! Form::close() !!}