<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth route
Route::get('/', 'AuthController@index')->name('default');
Route::post('/', 'AuthController@login')->name('default');
Route::get('/logout', 'AuthController@logout')->name('logout');
Route::get('/forgot', 'AuthController@formForgot')->name('forgotPass');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/ticket', 'TicketController@indexYourTicket')->name('ticket');
Route::get('/yticketnew', 'TicketController@indexYourNewTicket')->name('yTicketNew');
Route::get('/yticketprogress', 'TicketController@indexYourProgressTicket')->name('yTicketProgress');
Route::get('/yticketResolv', 'TicketController@indexYourResolvTicket')->name('yTicketResolv');
Route::get('/yticketreject', 'TicketController@indexYourRejectTicket')->name('yTicketReject');

Route::get('/sticket', 'TicketController@indexSTicket')->name('sTicket');
Route::get('/sticketnew', 'TicketController@indexSNewTicket')->name('sTicketNew');
Route::get('/sticketprogress', 'TicketController@indexSProgressTicket')->name('sTicketProgress');
Route::get('/sticketResolv', 'TicketController@indexSResolvTicket')->name('sTicketResolv');
Route::get('/sticketreject', 'TicketController@indexSRejectTicket')->name('sTicketReject');

Route::get('/aticket', 'TicketController@indexATicket')->name('aTicket');
Route::get('/aticketnew', 'TicketController@indexANewTicket')->name('aTicketNew');
Route::get('/aticketprogress', 'TicketController@indexAProgressTicket')->name('aTicketProgress');
Route::get('/aticketResolv', 'TicketController@indexAResolvTicket')->name('aTicketResolv');
Route::get('/aticketreject', 'TicketController@indexARejectTicket')->name('aTicketReject');

Route::get('/addticket', 'TicketController@formAdd')->name('addTicket');
Route::post('/addticket', 'TicketController@store')->name('addTicket');

Route::get('/modify/{id}', 'TicketController@formMod')->name('modifyTicket');
Route::post('/edit', 'TicketController@modStore')->name('postModTicket');

Route::get('/viewticket', 'TicketController@view')->name('viewTicket');

Route::get('/deleteticket/{id}', 'TicketController@suppr')->name('deleteTicket');


Route::post('/storetrace', 'TraceController@store')->name('storeTrace');

Route::get('/inventorylist', 'InventoryController@index')->name('inventory');

Route::get('/moduser', 'UserController@modForm')->name('account');
Route::post('/postmoduser', 'UserController@postModForm')->name('postModAccount');

/*
Route::get('/mdp', function () {
   echo \Illuminate\Support\Facades\Hash::make('ajpi@25032016');
}); */